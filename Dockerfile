FROM hypriot/rpi-node:4-slim

RUN npm install -g phant

VOLUME /phant_streams

EXPOSE 8080
EXPOSE 8081

ENTRYPOINT ["phant"]

CMD [""]
